import sbt._

object Version {
  lazy val scalaVersion = "2.12.11"
  lazy val scalaTest = "3.1.1"
  lazy val synaptixVersion = "0.1.11-SNAPSHOT"
  lazy val logback = "1.2.3"
  lazy val sttp = "1.7.2"
  lazy val playJson = "2.8.1"
  lazy val jodaTime = "2.10.5"
  lazy val tsConfig = "1.4.0"
  lazy val scalaLogging = "3.9.2"
  lazy val akkaVersion = "2.6.4"
  lazy val quartz = "2.3.1"
  lazy val elastic4sVersion = "7.6.1"
}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val typesafeConfig = "com.typesafe" % "config" % Version.tsConfig
  lazy val jodaTime = "joda-time" % "joda-time" % Version.jodaTime

  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akkaVersion
  lazy val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % Version.akkaVersion
  lazy val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % Version.akkaVersion

  lazy val playJson = "com.typesafe.play" %% "play-json" % Version.playJson
  lazy val sttpCore = "com.softwaremill.sttp" %% "core" % Version.sttp

  val quartz = "org.quartz-scheduler" % "quartz" % "2.3.2"

  val synaptixIndexingToolkit = "com.mnemotix" %% "synaptix-indexing-toolkit" % Version.synaptixVersion
  val synaptixRdfToolkit = "com.mnemotix" %% "synaptix-rdf-toolkit" % Version.synaptixVersion

//  val elastic4sTestKit = "com.sksamuel.elastic4s" %% "elastic4s-testkit" % Version.elastic4sVersion % "test"

}
