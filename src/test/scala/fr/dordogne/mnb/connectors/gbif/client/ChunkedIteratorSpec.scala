/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.gbif.client

import java.nio.charset.StandardCharsets
import java.nio.file.Files

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.mnemotix.synaptix.rdf.client.RDFFormats
import com.mnemotix.synaptix.rdf.client.rdf4j.Rdf4jRDFClient
import com.softwaremill.sttp._
import fr.dordogne.mnb.commons.ChunkedIterator
import fr.dordogne.mnb.connectors.gbif.models.{ GbifResponse, GbifResult }
import fr.dordogne.mnb.connectors.{ HttpClient, RDFSerializer, TestSpec }
import org.joda.time.DateTime
import play.api.libs.json.Json

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext, Future }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 18/06/2020
 */

class ChunkedIteratorSpec extends TestSpec {

  implicit val system = ActorSystem("ChunkedIteratorSpec-" + System.currentTimeMillis())
  implicit val materializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  val minusDays = 2
  val timestamp = DateTime.now().toString("yyyyMMdd")

  // RDF client init
  val rdf4jClient = new Rdf4jRDFClient
  val repoName = "dev-crash-mnb"
  rdf4jClient.init()
  implicit val writeConn = rdf4jClient.getWriteConnection(repoName)

  // GBif client init
  val client = new HttpClient {
    override val accessPointUri: String = "https://api.gbif.org"
    val dateFrom = DateTime.now().minusDays(minusDays)
    val geometry = "POLYGON((0.62974 45.71457,0.30267 45.45906,0.2666 45.29775,-0.01785 45.16922,0.07329 45.07012,-0.03421 44.85211,0.28515 44.86456,0.35459 44.65481,0.79772 44.70177,0.8425 44.60096,1.10321 44.57173,1.44193 44.87758,1.41289 45.12509,1.22781 45.20603,1.32279 45.38266,1.02333 45.60893,0.8115 45.57587,0.62974 45.71457))"
    val dateDeb: String = dateFrom.toString("yyyy-MM-dd")
    val uri = uri"$accessPointUri/v1/occurrence/search?has_coordinate=true&has_geospatial_issue=false&year=*,*&lastInterpreted=${dateDeb},*&geometry=$geometry&offset=0&limit=1"
  }

  val iterator: Iterator[String] = new ChunkedIterator[String] {
    override val chunkSize: Int = 1000
    override val idle: Long = 5000L
    val maxItems = 2000

    override def hasNext: Boolean = {
      (currentChunk < nbChunks && currentChunk * chunkSize < maxItems)
    }

    override val nbItems: Int = {
      val resp = client.get(client.uri)
      val json = Json.parse(resp).as[GbifResponse]
      val count = json.count
      logger.debug(count.toString)
      count
    }

    override def next(): String = {
      val offset = currentChunk * chunkSize
      currentChunk += 1
      if (idle > 0) Thread.sleep(idle)
      getChunk(offset, chunkSize)
    }

    override def getChunk(offset: Int, size: Int): String = {
      val uri = uri"${client.accessPointUri}/v1/occurrence/search?has_coordinate=true&has_geospatial_issue=false&year=*,*&lastInterpreted=${client.dateDeb},*&geometry=${client.geometry}&offset=${offset}&limit=${size}"
      logger.debug(uri.toString)
      client.get(uri)
    }
  }

  "shoud iterate over api" in {

    val model = RDFSerializer.getModel().build()

    val src: Source[Seq[GbifResult], _] = Source.fromIterator(() => iterator.map(s => Json.parse(s).as[GbifResponse].results))
    val f: Future[Done] = src.runForeach { seq =>
      seq.foreach { res =>
        RDFSerializer.merge(model, GbifResult.toModel(res))
      }
    }
    f.futureValue

    // Write data to the RDF Repository
    val filepath = Files.createTempFile("gbif-", s"-$timestamp-minus-$minusDays.trig")
    logger.debug(filepath.toAbsolutePath.toString)
    Files.write(filepath, RDFSerializer.toTriG(model).getBytes(StandardCharsets.UTF_8))
    Await.result(rdf4jClient.load(filepath.toFile, None, RDFFormats.TRIG)(executionContext, writeConn), Duration.Inf)
  }
}
