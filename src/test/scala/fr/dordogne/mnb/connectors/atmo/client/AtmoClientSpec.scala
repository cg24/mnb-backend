/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.atmo.client

import fr.dordogne.mnb.connectors.TestSpec
import fr.dordogne.mnb.connectors.atmo.models.FeatureCollection
import org.joda.time.DateTime
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

class AtmoClientSpec extends TestSpec {

  val client = new AtmoClient

  "shoud connect to api" in {
    val json = Json.parse(client.indice(DateTime.now().minusDays(30), DateTime.now()))
    val parsing = json.validate[FeatureCollection]
    parsing.isSuccess shouldBe true
    val featureColl = parsing.get
    println(Json.prettyPrint(Json.toJson(featureColl)))
  }
}
