/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.hubeau.client

import akka.dispatch.ExecutionContexts
import com.mnemotix.synaptix.core.utils.CryptoUtils
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import fr.dordogne.mnb.commons.IndexingClient
import fr.dordogne.mnb.connectors.TestSpec
import fr.dordogne.mnb.connectors.hubeau.models.NiveauNappe
import org.joda.time.DateTime
import play.api.libs.json.{ JsObject, Json }

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.Try

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

class HubeauNappesClientSpec extends TestSpec {

  implicit lazy val ec: ExecutionContext = ExecutionContexts.global()
  //  val indexName = "dev-mnb-nappes-" + DateTime.now().toString("yyyyMMddHHmm")
  val indexName = "dev-mnb-nappes"

  "HubeauNappesClient" should {

    val client = new HubeauNappesClient

    "get station codes from api" in {
      val bbox = "-0.042,44.5707,1.4483,45.7146"

      val data: Seq[ESIndexable] = client.codeStations(bbox).flatMap { code =>
        //        Thread.sleep(500)
        val json = Json.parse(client.niveauxNappes(code, DateTime.now().minusMonths(12)))
        val parsing = json.validate[NiveauNappe]
        parsing.isSuccess shouldBe true
        val nivnappe = parsing.get
        println(s"$code : ${nivnappe.count} measures ${if (nivnappe.count > 1000) "<== exceeded page size"}")
        println(nivnappe.count)
        nivnappe.data.map { measure =>
          ESIndexable(
            CryptoUtils.md5sum(code, measure.date_mesure),
            Some(Json.obj(
              "bssCode" -> code,
              "date_mesure" -> measure.date_mesure,
              "niveau_nappe_eau" -> measure.niveau_nappe_eau)))
        }
      }
      IndexClient.bulkInsert(indexName, data: _*).futureValue
      logger.info(s"${data.size} items indexed")
    }
  }

  override protected def beforeAll(): Unit = {
    logger.info(s"Index $indexName creation")
    Try(IndexingClient.deleteIndex(indexName).futureValue)
    Thread.sleep(3000)
    Try(IndexingClient.createIndex(indexName, IndexingClient.hubeauNappesMappingDef).futureValue)
    logger.info(s"Index $indexName created successfully")
    Thread.sleep(2000)
  }
}
