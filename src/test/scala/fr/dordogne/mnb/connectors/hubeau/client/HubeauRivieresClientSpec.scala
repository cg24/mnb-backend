/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.hubeau.client

import akka.dispatch.ExecutionContexts
import com.mnemotix.synaptix.core.utils.CryptoUtils
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.indexes.IndexResponse
import fr.dordogne.mnb.commons.IndexingClient
import fr.dordogne.mnb.connectors.TestSpec
import fr.dordogne.mnb.connectors.hubeau.models.{ NiveauNappe, NiveauRiviere }
import org.joda.time.DateTime
import play.api.libs.json.{ JsObject, Json }

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.Try

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

class HubeauRivieresClientSpec extends TestSpec {

  implicit lazy val ec: ExecutionContext = ExecutionContexts.global()
  //  val indexName = "dev-mnb-rivieres-" + DateTime.now().toString("yyyyMMddHHmm")
  val indexName = "dev-mnb-rivieres"

  "HubeauRivieresClient" should {

    val client = new HubeauRivieresClient

    "get station codes from api" in {

      val data: Seq[ESIndexable] = client.codeStations().flatMap { code =>
        val json = Json.parse(client.niveauxRivieres(code, DateTime.now().minusDays(30)))
        val parsing = json.validate[NiveauRiviere]
        parsing.isSuccess shouldBe true
        val nivriviere = parsing.get
        println(s"$code : ${nivriviere.count} measures ${if (nivriviere.count > 1000) "<== exceeded page size"}")
        nivriviere.data.map { measure =>
          ESIndexable(
            CryptoUtils.md5sum(code, measure.date_obs),
            Some(Json.obj(
              "codeSite" -> code,
              "date_obs" -> measure.date_obs,
              "resultat_obs" -> measure.resultat_obs)))
        }
      }
      IndexClient.bulkInsert(indexName, data: _*).futureValue
      logger.info(s"${data.size} items indexed")
    }
  }

  override protected def beforeAll(): Unit = {
    logger.info(s"Index $indexName creation")
    Try(IndexingClient.deleteIndex(indexName).futureValue)
    Thread.sleep(3000)
    IndexingClient.createIndex(indexName, IndexingClient.hubeauRivieresMappingDef).futureValue
    logger.info(s"Index $indexName created successfully")
    Thread.sleep(2000)
  }
}
