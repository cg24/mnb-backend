/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors

import java.io.ByteArrayOutputStream
import java.util.Date

import org.eclipse.rdf4j.model.impl.{ SimpleNamespace, SimpleValueFactory }
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.{ IRI, Literal, Model, ValueFactory }
import org.eclipse.rdf4j.rio.Rio

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 10/02/2020
 */

object RDFSerializer {

  val vf: ValueFactory = SimpleValueFactory.getInstance

  val mnxOntologyPrefix: String = "http://ns.mnemotix.com/ontologies/2019/8/generic-model/"

  val mnbOntologyPrefix: String = "http://mnb.dordogne.fr/ontology/"
  val mnbDataPrefix: String = "http://mnb.dordogne.fr/data/"
  val mnbVocabularyPrefix: String = "http://mnb.dordogne.fr/vocabulary/"
  val gbifDataPrefix: String = "https://www.gbif.org/occurrence/"
  val gbifDatasetPrefix: String = "https://www.gbif.org/dataset/"

  val skosOntologyPrefix: String = "http://www.w3.org/2004/02/skos/core#"
  val provOntologyPrefix: String = "http://www.w3.org/ns/prov#"
  val geosparqlOntologyPrefix: String = "http://www.opengis.net/ont/geosparql#"
  val sosaOntologyPrefix: String = "http://www.w3.org/ns/sosa/"

  val clrVocPrefix: String = "http://data.clairsienne.com/data/2019/12/vocabulary/"
  val clrDataPrefix: String = "http://data.clairsienne.com/data/2019/12/clr-patrimoine/"

  val mnxNS = new SimpleNamespace("mnx", mnxOntologyPrefix)
  val mnbNS = new SimpleNamespace("mnb", mnbOntologyPrefix)
  val mnbvNS = new SimpleNamespace("mnbv", mnbVocabularyPrefix)
  val mnbdNS = new SimpleNamespace("mnbd", mnbDataPrefix)
  val sosaNS = new SimpleNamespace("sosa", sosaOntologyPrefix)
  val gbifNS = new SimpleNamespace("gbif", gbifDataPrefix)
  val gbifDsNS = new SimpleNamespace("gbifds", gbifDatasetPrefix)
  val provNS = new SimpleNamespace("prov", provOntologyPrefix)
  val geosparqlNS = new SimpleNamespace("geosparql", geosparqlOntologyPrefix)
  val defaultNS = new SimpleNamespace("", mnbDataPrefix)

  def getModel(model: Option[Model] = None) = {

    import org.eclipse.rdf4j.model.vocabulary.XMLSchema

    val builder = model.map(new ModelBuilder(_)).getOrElse(new ModelBuilder())
    builder
      .setNamespace(defaultNS)
      .setNamespace(XMLSchema.NS)
      .setNamespace(mnbNS)
      .setNamespace(mnbvNS)
      .setNamespace(mnbdNS)
      .setNamespace(mnxNS)
      .setNamespace(sosaNS)
      .setNamespace(gbifNS)
      .setNamespace(gbifDsNS)
      .setNamespace(provNS)
      .setNamespace(geosparqlNS)
  }

  def merge(m1: Model, m2: Model): Model = {
    m1.getNamespaces.addAll(m2.getNamespaces)
    m1.addAll(m2)
    m1
  }

  def createIRI(id: String, prefix: Option[String] = None): IRI = {
    vf.createIRI(prefix.getOrElse(""), id)
  }

  def createMnxIRI(id: String): IRI = {
    vf.createIRI(mnxOntologyPrefix, id)
  }

  def createProvIRI(id: String): IRI = {
    vf.createIRI(provOntologyPrefix, id)
  }

  def createGeoSparqlIRI(id: String): IRI = {
    vf.createIRI(geosparqlOntologyPrefix, id)
  }

  def createLiteral(value: String): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Int): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Date): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Long): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Float): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Short): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Double): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Boolean): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: java.math.BigDecimal): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: java.math.BigInteger): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(label: String, datatype: IRI): Literal = {
    vf.createLiteral(label, datatype)
  }

  def createLocalizedLabel(label: String, language: String): Literal = {
    vf.createLiteral(label, language)
  }

  def toTriG(model: Model) = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    Rio.write(model, os, org.eclipse.rdf4j.rio.RDFFormat.TRIG)
    //    val bytes = os.toByteArray
    val bytes = os.toString
    os.close()
    bytes
  }

  def toJsonLd(model: Model) = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    Rio.write(model, os, org.eclipse.rdf4j.rio.RDFFormat.JSONLD)
    //    val bytes = os.toByteArray
    val bytes = os.toString
    os.close()
    bytes
  }

}
