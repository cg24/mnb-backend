/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors

import com.softwaremill.sttp._
import com.typesafe.scalalogging.LazyLogging
import fr.dordogne.mnb.commons.HttpClientException

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

trait HttpClient extends LazyLogging {

  implicit val backend = HttpURLConnectionBackend()

  val accessPointUri: String
  val defaultHeaders = Map("Accept" -> "application/json")

  def get(service: Uri, headers: Map[String, String] = defaultHeaders, credentials: Option[Credentials] = None) = {
    val request = if (credentials.isDefined) sttp.auth.basic(credentials.get.login, credentials.get.password).headers(headers).get(service) else sttp.headers(headers).get(service)
    val response = request.send()
    if (response.body.isLeft) {
      logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
      throw HttpClientException(response.body.left.get, null)
    } else {
      response.body.right.get
    }
  }
}

