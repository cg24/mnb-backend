/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.gbif.client

import com.softwaremill.sttp._
import com.typesafe.scalalogging.LazyLogging
import fr.dordogne.mnb.commons.ChunkedIterator
import fr.dordogne.mnb.connectors.HttpClient
import fr.dordogne.mnb.connectors.gbif.models.GbifResponse
import org.joda.time.DateTime
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 29/09/2020
 */

class GbifClient extends ChunkedIterator[String] with LazyLogging {

  override val chunkSize: Int = GbifClientConfiguration.chunkSize
  override val idle: Long = GbifClientConfiguration.idleTime
  val minusDays = GbifClientConfiguration.minusDays
  val maxItems = GbifClientConfiguration.maxItems
  val geometry = GbifClientConfiguration.geometry

  val client = new HttpClient {
    override val accessPointUri: String = GbifClientConfiguration.uri
    val dateFrom = DateTime.now().minusDays(minusDays)
    val dateDeb: String = dateFrom.toString("yyyy-MM-dd")
    val uri = uri"$accessPointUri/v1/occurrence/search?has_coordinate=true&has_geospatial_issue=false&year=*,*&lastInterpreted=${dateDeb},*&geometry=$geometry&offset=0&limit=1"
  }

  override def hasNext: Boolean = (currentChunk < nbChunks && currentChunk * chunkSize < maxItems)

  override val nbItems: Int = {
    val resp = client.get(client.uri)
    val json = Json.parse(resp).as[GbifResponse]
    val count = json.count
    logger.info(s"$count items found")
    count
  }

  override def next(): String = {
    val offset = currentChunk * chunkSize
    currentChunk += 1
    if (idle > 0) Thread.sleep(idle)
    getChunk(offset, chunkSize)
  }

  override def getChunk(offset: Int, size: Int): String = {
    val uri = uri"${client.accessPointUri}/v1/occurrence/search?has_coordinate=true&has_geospatial_issue=false&year=*,*&lastInterpreted=${client.dateDeb},*&geometry=${geometry}&offset=${offset}&limit=${size}"
    logger.info(uri.toString)
    client.get(uri)
  }
}
