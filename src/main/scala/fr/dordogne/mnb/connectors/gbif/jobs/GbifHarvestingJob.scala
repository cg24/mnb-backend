/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.gbif.jobs

import java.nio.charset.StandardCharsets
import java.nio.file.Files

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.Source
import com.mnemotix.synaptix.rdf.client.RDFFormats
import com.mnemotix.synaptix.rdf.client.rdf4j.Rdf4jRDFClient
import com.typesafe.scalalogging.LazyLogging
import fr.dordogne.mnb.connectors.RDFSerializer
import fr.dordogne.mnb.connectors.gbif.client.{ GbifClient, GbifClientConfiguration }
import fr.dordogne.mnb.connectors.gbif.models.{ GbifResponse, GbifResult }
import org.joda.time.DateTime
import org.quartz.{ Job, JobExecutionContext }
import play.api.libs.json.Json

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext, Future }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 29/09/2020
 */

class GbifHarvestingJob extends Job with LazyLogging {

  val repoName = GbifClientConfiguration.repoName

  def run() = {
    implicit val system = ActorSystem("GbifHarvestingJob-" + System.currentTimeMillis())
    implicit val executionContext: ExecutionContext = system.dispatcher

    // HTTP client init
    val client = new GbifClient()

    // RDF client init
    val rdf4jClient = new Rdf4jRDFClient
    rdf4jClient.init()
    implicit val writeConn = rdf4jClient.getWriteConnection(repoName)
    val model = RDFSerializer.getModel().build()

    // API Harvesting
    val src: Source[Seq[GbifResult], _] = Source.fromIterator(() => client.map(s => Json.parse(s).as[GbifResponse].results))
    val f: Future[Done] = src.runForeach { seq =>
      seq.foreach { res =>
        RDFSerializer.merge(model, GbifResult.toModel(res))
      }
    }
    Await.result(f, Duration.Inf)

    // Create a temporary file
    val timestamp = DateTime.now().toString("yyyyMMdd")
    val filepath = Files.createTempFile("gbif-", s"-$timestamp-minus-${client.minusDays}.trig")
    logger.info(s"Temporary file created ${filepath.toAbsolutePath.toString}")
    Files.write(filepath, RDFSerializer.toTriG(model).getBytes(StandardCharsets.UTF_8))

    // Load data to the RDF Repository
    Await.result(rdf4jClient.load(filepath.toFile, None, RDFFormats.TRIG)(executionContext, writeConn), Duration.Inf)
  }

  override def execute(context: JobExecutionContext): Unit = run()
}
