/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.gbif.models

import java.util.Date

import org.eclipse.rdf4j.model.Model
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/06/2020
 */

case class GbifResponse(
  offset: Int,
  limit: Int,
  endOfRecords: Boolean,
  count: Int,
  results: Seq[GbifResult])

case class GbifResult(
  key: Long,
  datasetKey: String,
  eventDate: String,
  taxonKey: Long,
  decimalLongitude: BigDecimal,
  decimalLatitude: BigDecimal)

object GbifResult {
  implicit lazy val format = Json.format[GbifResult]

  def uri(id: Long): String = s"gbif:${id}"

  def uri(entity: GbifResult): String = uri(entity.key)

  def toModel(entity: GbifResult): Model = {
    import fr.dordogne.mnb.connectors.RDFSerializer._
    import org.eclipse.rdf4j.model.vocabulary.RDF

    val eventDate: Date = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").parseDateTime(entity.eventDate).toDate
    val pointUri = s"gbif:${entity.key}#point"
    val pointWKT = s"POINT(${entity.decimalLongitude} ${entity.decimalLatitude})"

    val builder = getModel()
      .subject(pointUri)
      .add(RDF.TYPE, "mnx:Geometry")
      .add("geosparql:asWKT", createLiteral(pointWKT, createGeoSparqlIRI("wktLiteral")))
      .subject(uri(entity))
      .add(RDF.TYPE, "mnb:TaxonOccurrence")
      .add("sosa:resultTime", createLiteral(eventDate))
      .add("mnx:hasGeometry", pointUri)
      .add("prov:hadPrimarySource", createIRI(s"https://www.gbif.org/dataset/${entity.datasetKey}"))
      .add("mnb:gbifTaxonKey", createLiteral(entity.taxonKey.toString))

    builder.build()
  }
}

object GbifResponse {
  implicit lazy val format = Json.format[GbifResponse]
}