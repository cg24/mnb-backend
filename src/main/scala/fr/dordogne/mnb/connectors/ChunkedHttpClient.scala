/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors

import com.softwaremill.sttp._
import com.typesafe.scalalogging.LazyLogging
import fr.dordogne.mnb.commons.HttpClientException

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

trait ChunkedHttpClient extends Iterator[String] with LazyLogging {

  implicit val backend = HttpURLConnectionBackend()

  val chunkSize: Int
  val idle: Int = 0
  val credentials: Option[Credentials] = None
  val headers: Map[String, String] = Map("Accept" -> "application/json")

  val nbItems: Int = getNbitems()

  lazy val nbChunks: Int = Math.ceil(nbItems.toDouble / chunkSize.toDouble).toInt

  var currentChunk = 0

  override def hasNext: Boolean = currentChunk < nbChunks

  override def next(): String = {
    val offset = currentChunk * chunkSize
    currentChunk += 1
    if (idle > 0) Thread.sleep(idle)
    getChunk(offset)
  }

  def getNbitems(): Int

  def getServiceUri(offset: Int, chunkSize: Int): Uri

  private def getChunk(offset: Int) = {
    val request = if (credentials.isDefined) sttp.auth.basic(credentials.get.login, credentials.get.password).headers(headers).get(getServiceUri(offset, chunkSize)) else sttp.headers(headers).get(getServiceUri(offset, chunkSize))
    val response = request.send()
    if (response.body.isLeft) {
      logger.error(s"${response.statusText}(${response.code}): ${response.body.left.get}")
      throw HttpClientException(response.body.left.get, null)
    } else {
      response.body.right.get
    }
  }
}

