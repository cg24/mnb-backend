/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.atmo.client

import fr.dordogne.mnb.connectors.HttpClient
import com.softwaremill.sttp._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

class AtmoClient extends HttpClient {
  override val accessPointUri: String = "https://opendata.atmo-na.org/api/v1"

  def indice(dateFrom: DateTime, dateTo: DateTime) = {
    val dateDeb = DateTimeFormat.forPattern("yyyy-MM-dd").print(dateFrom)
    val dateFin = DateTimeFormat.forPattern("yyyy-MM-dd").print(dateTo)
    get(uri"$accessPointUri/indice/atmo/?date_deb=$dateDeb&date_fin=$dateFin&zone=24322&type=json")
  }
}
