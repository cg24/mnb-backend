/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.atmo.models

import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/06/2020
 */

case class Properties(
  code_zone: String,
  lib_zone: String,
  date_ech: String,
  valeur: Int,
  `type`: String)

object Properties {
  implicit lazy val format = Json.format[Properties]
}

case class Feature(`type`: String, properties: Properties)

object Feature {
  implicit lazy val format = Json.format[Feature]
}

case class FeatureCollection(
  totalFeatures: Int,
  returnFeatures: Int,
  offset: Int,
  features: Seq[Feature])

object FeatureCollection {
  implicit lazy val format = Json.format[FeatureCollection]
}
