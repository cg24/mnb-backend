/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.hubeau.jobs

import akka.dispatch.ExecutionContexts
import com.mnemotix.synaptix.core.utils.CryptoUtils
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.typesafe.scalalogging.LazyLogging
import fr.dordogne.mnb.commons.{ IndexingClient, ParsingException }
import fr.dordogne.mnb.connectors.hubeau.client.{ HubeauClientConfiguration, HubeauRivieresClient }
import fr.dordogne.mnb.connectors.hubeau.models.NiveauRiviere
import org.joda.time.DateTime
import org.quartz.{ Job, JobExecutionContext }
import play.api.libs.json.Json

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext }
import scala.util.{ Failure, Success }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 27/07/2020
 */

class HubeauRivieresHarvestingJob extends Job with LazyLogging {

  implicit lazy val ec: ExecutionContext = ExecutionContexts.global()

  val indexName = HubeauClientConfiguration.hydrometrieIndexName

  def run() = {
    logger.info(s"Index $indexName creation")
    val f = IndexingClient.createIndex(indexName, IndexingClient.hubeauNappesMappingDef)
    f.onComplete {
      case Success(_) => logger.info(s"Index $indexName created successfully")
      case Failure(ex) => logger.warn(s"Index $indexName already exists.")
    }
    Await.result(f, Duration.Inf)
    Thread.sleep(2000)

    val client = new HubeauRivieresClient
    val data: Seq[ESIndexable] = client.codeStations().flatMap { code =>
      Thread.sleep(1000) // Timer pour éviter la surcharge de l'API
      val json = Json.parse(client.niveauxRivieres(code, DateTime.now().minusDays(HubeauClientConfiguration.hydrometrieMinusDays)))
      val parsing = json.validate[NiveauRiviere]
      if (parsing.isSuccess) {
        val nivriviere = parsing.get
        logger.info(s"$code : ${nivriviere.count} measures ${if (nivriviere.count > 1000) "<== exceeded page size"}")
        nivriviere.data.map { measure =>
          ESIndexable(
            CryptoUtils.md5sum(code, measure.date_obs),
            Some(Json.obj(
              "codeSite" -> code,
              "date_obs" -> measure.date_obs,
              "resultat_obs" -> measure.resultat_obs)))
        }
      } else throw ParsingException("Impossible de parser le résultat renvoyé par l'API", null)
    }
    Await.result(IndexClient.bulkInsert(indexName, data: _*), Duration.Inf)
    logger.info(s"${data.size} items indexed")
  }

  override def execute(context: JobExecutionContext): Unit = run()

}
