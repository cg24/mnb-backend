/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.hubeau.models

import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 05/06/2020
 */

case class StationsNappe(
  count: Int,
  api_version: String,
  data: Seq[StationNappe])

case class StationNappe(
  code_bss: String)

object StationNappe {
  implicit lazy val format = Json.format[StationNappe]
}

object StationsNappe {
  implicit lazy val format = Json.format[StationsNappe]
}

case class NiveauNappe(
  count: Int,
  api_version: String,
  data: Seq[MesureNappe])

case class MesureNappe(
  date_mesure: String,
  niveau_nappe_eau: Float)

object MesureNappe {
  implicit lazy val format = Json.format[MesureNappe]
}

object NiveauNappe {
  implicit lazy val format = Json.format[NiveauNappe]
}
