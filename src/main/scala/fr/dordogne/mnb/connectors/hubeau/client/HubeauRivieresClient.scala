/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.hubeau.client

import com.softwaremill.sttp._
import fr.dordogne.mnb.connectors.HttpClient
import fr.dordogne.mnb.connectors.hubeau.models.StationsRivieres
import org.joda.time.DateTime
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

class HubeauRivieresClient extends HttpClient {
  override val accessPointUri: String = "https://hubeau.eaufrance.fr/api/v1"

  def codeStations(): Seq[String] = {
    val response = get(uri"$accessPointUri/hydrometrie/referentiel/stations?code_departement=24&en_service=true")
    val json = Json.parse(response)
    json.as[StationsRivieres].data.map(_.code_site)
  }

  def niveauxRivieres(codeSite: String, dateFrom: DateTime, size: Int = 1000) = {
    val dateStr = dateFrom.toString("yyyy-MM-dd'")
    val response = get(uri"$accessPointUri/hydrometrie/observations_tr?code_entite=$codeSite&date_debut_obs=$dateStr&size=$size&grandeur_hydro=H&timestep=60&fields=resultat_obs,date_obs")
    response
  }

}
