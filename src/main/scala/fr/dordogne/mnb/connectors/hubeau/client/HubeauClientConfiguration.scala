/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.connectors.hubeau.client

import com.typesafe.config.{ Config, ConfigFactory }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 29/09/2020
 */

object HubeauClientConfiguration {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("hubeau")).getOrElse(ConfigFactory.empty())
  lazy val nappesMinusMonthes: Int = conf.getInt("nappes.minus.monthes")
  lazy val nappesBbox: String = conf.getString("nappes.bbox")
  lazy val nappesIndexName: String = conf.getString("nappes.index.name")
  lazy val hydrometrieMinusDays: Int = conf.getInt("hydrometrie.minus.days")
  lazy val hydrometrieIndexName: String = conf.getString("hydrometrie.index.name")
}
