/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.commons

import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.ESMappingDefinitions
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 10/07/2020
 */

object IndexingClient extends LazyLogging {

  val hubeauNappesMappingDef = ESMappingDefinitions(
    Json.parse(
      """
        |{
        |    "properties": {
        |      "bssCode": { "type": "keyword" },
        |      "date_mesure": { "type": "date" },
        |      "niveau_nappe_eau": { "type": "float" }
        |    }
        |  }
        |""".stripMargin))

  val hubeauRivieresMappingDef = ESMappingDefinitions(
    Json.parse(
      """
        |{
        |    "properties": {
        |      "codeSite": { "type": "keyword" },
        |      "date_obs": { "type": "date" },
        |      "resultat_obs": { "type": "integer" }
        |    }
        |  }
        |""".stripMargin))

  def createIndex(indexName: String, mappings: ESMappingDefinitions)(implicit ec: ExecutionContext) = {
    IndexClient.createIndex(indexName, mappings.toMappingDefinition())
  }

  def deleteIndex(indexName: String)(implicit ec: ExecutionContext) = {
    IndexClient.deleteIndex(indexName)
  }
}
