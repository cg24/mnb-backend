/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.commons

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-06-20
 */

trait ChunkedIterator[itemType] extends Iterator[itemType] {

  val chunkSize: Int
  val nbItems: Int
  val idle: Long

  lazy val nbChunks: Int = Math.ceil(nbItems.toDouble / chunkSize.toDouble).toInt
  var currentChunk = 0

  override def hasNext: Boolean = (currentChunk < nbChunks)

  override def next(): itemType = {
    val offset = currentChunk * chunkSize
    currentChunk += 1
    if (idle > 0) Thread.sleep(idle)
    getChunk(offset, chunkSize)
  }

  def getChunk(offset: Int, size: Int): itemType
}
