/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb

import fr.dordogne.mnb.connectors.gbif.jobs.GbifHarvestingJob
import fr.dordogne.mnb.connectors.hubeau.jobs.{ HubeauNappesHarvestingJob, HubeauRivieresHarvestingJob }
import fr.dordogne.mnb.scheduler.HarvesterConfiguration
import org.quartz.impl.StdSchedulerFactory
import org.quartz.{ CronScheduleBuilder, JobBuilder, Trigger, TriggerBuilder }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

object JobRunner extends App {

  // run the first harvexing task on launch
  var rivieresTask = new HubeauRivieresHarvestingJob
  var nappesTask = new HubeauNappesHarvestingJob
  var GbifTask = new GbifHarvestingJob

  rivieresTask.run()
  nappesTask.run()
  GbifTask.run()

  val quartz = StdSchedulerFactory.getDefaultScheduler

  val rivieresJob = JobBuilder.newJob(classOf[HubeauRivieresHarvestingJob]).withIdentity("HubeauRivieresHarvestingJob", "MNB").build
  val nappesJob = JobBuilder.newJob(classOf[HubeauNappesHarvestingJob]).withIdentity("HubeauNappesHarvestingJob", "MNB").build
  val gbifJob = JobBuilder.newJob(classOf[GbifHarvestingJob]).withIdentity("GbifHarvestingJob", "MNB").build

  //  val trigger: Trigger = TriggerBuilder.newTrigger.withIdentity("Trigger", "MNB").withSchedule(CronScheduleBuilder.cronSchedule(CrontabExpressionHelper.EVERY_DAY_AT_6_AM)).build
  val triggerRivieres: Trigger = TriggerBuilder.newTrigger.withIdentity("Trigger1", "MNB").withSchedule(CronScheduleBuilder.cronSchedule(HarvesterConfiguration.cronExpression)).build
  val triggerNappes: Trigger = TriggerBuilder.newTrigger.withIdentity("Trigger2", "MNB").withSchedule(CronScheduleBuilder.cronSchedule(HarvesterConfiguration.cronExpression)).build
  val triggerGbif: Trigger = TriggerBuilder.newTrigger.withIdentity("Trigger3", "MNB").withSchedule(CronScheduleBuilder.cronSchedule(HarvesterConfiguration.cronExpression)).build

  quartz.start
  quartz.scheduleJob(rivieresJob, triggerRivieres)
  quartz.scheduleJob(nappesJob, triggerNappes)
  quartz.scheduleJob(gbifJob, triggerGbif)

}
