/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dordogne.mnb.scheduler

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 08/06/2020
 */

object CrontabExpressionHelper {
  /*
  More expressions at https://www.freeformatter.com/cron-expression-generator-quartz.html
     */
  val EVERY_SECOND = "* * * ? * *"
  val EVERY_5_SECONDS = "*/5 * * ? * *"
  val EVERY_10_SECONDS = "0/10 0 0 ? * * *"
  val EVERY_MINUTE = "0 * * ? * *"
  val EVERY_5_MINUTES = "0 */5 * ? * *"
  val EVERY_10_MINUTES = "0 */10 * ? * *"
  val EVERY_HOUR = "0 0 * ? * *"
  val EVERY_DAY_AT_MIDNIGHT = "0 0 0 * * ?"
  val EVERY_DAY_AT_6_AM = "0 0 6 * * ?"

}