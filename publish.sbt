import sbt.url

ThisBuild / scalaVersion := Version.scalaVersion
ThisBuild / organization := "fr.dordogne"
ThisBuild / organizationName := "Conseil Départemental de Dordogne"
ThisBuild / licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / developers := List(
  Developer(
    id = "ndelaforge",
    name = "Nicolas Delaforge",
    email = "nicolas.delaforge@mnemotix.com",
    url = url("http://www.mnemotix.com")
  )
)

ThisBuild / credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
ThisBuild / updateOptions := updateOptions.value.withGigahorse(false)
// Remove all additional repository other than Maven Central from POM
ThisBuild / pomIncludeRepository := { _ => false }
ThisBuild / publishArtifact := true
ThisBuild / publishMavenStyle := true
ThisBuild / publishTo := {
  val nexus = "https://nexus.mnemotix.com/repository"
  if (isSnapshot.value) Some("MNX Nexus" at nexus + "/maven-snapshots/")
  else Some("MNX Nexus" at nexus + "/maven-releases/")
}

ThisBuild / resolvers ++= Seq(
  Resolver.mavenLocal,
  Resolver.sonatypeRepo("public"),
  Resolver.typesafeRepo("releases"),
  Resolver.typesafeIvyRepo("releases"),
  Resolver.sbtPluginRepo("releases"),
  Resolver.bintrayRepo("owner", "repo"),
  "MNX Nexus (releases)" at "https://nexus.mnemotix.com/repository/maven-releases/",
  "MNX Nexus (snapshots)" at "https://nexus.mnemotix.com/repository/maven-snapshots/"
)

