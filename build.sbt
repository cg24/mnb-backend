import Dependencies._

val meta = """META.INF(.)*""".r

lazy val root = (project in file("."))
  .settings(
    name := "mnb-backend",
    description := "Middleware sémantique pour la Maison Numérique de la Biodiversité",
    homepage := Some(url("https://gitlab.com/cg24/mnb-backend")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/cg24/mnb-backend.git"),
        "scm:git@gitlab.com:cg24/mnb-backend.git"
      )
    ),
    libraryDependencies ++= Seq(
      scalaTest % Test,
      scalaLogging,
      logbackClassic,
      typesafeConfig,
      jodaTime,
      playJson,
      akkaStream,
      akkaStreamTestkit % Test,
      akkaSlf4j,
      sttpCore,
      synaptixIndexingToolkit,
      synaptixRdfToolkit,
      quartz
    ),
    test in assembly := {},
    assemblyMergeStrategy in assembly := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    mainClass in assembly := Some("fr.dordogne.mnb.JobRunner"),
    assemblyJarName in assembly := "mnb-backend.jar",
    packageName in Docker := packageName.value,
    version in Docker := version.value,
    dockerExposedPorts ++= Seq(8080),
    dockerRepository := Some("registry.gitlab.com/cg24"),

  ).enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)
  .enablePlugins(AshScriptPlugin)
